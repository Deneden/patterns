﻿using System;

namespace Patterns
{
    public class Menu
    {
        private readonly Pattern[] _elements;

        public Menu(Pattern[] elements)
        {
            _elements = elements;
        }
        public void Display()
        {
            for (var i=0; i < _elements.Length; i++)
            {
                Console.WriteLine($"\t{i+1} - {_elements[i].GetName()}");
            }

            Console.WriteLine();
            Console.WriteLine("\t0 - Exit");
        }
    }
}

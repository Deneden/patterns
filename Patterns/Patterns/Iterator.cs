﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Patterns.Patterns
{
    public class Iterator : Pattern
    {
        public override string GetName()
        {
            return "Iterator";
        }

        public override string GetDescription()
        {
            return
                "Паттерн Итератор (Iterator) предоставляет абстрактный интерфейс для последовательного доступа ко всем элементам составного объекта без раскрытия его внутренней структуры." +
                "\n\n - Когда использовать итераторы?" +
                "\n - Когда необходимо осуществить обход объекта без раскрытия его внутренней структуры" +
                "\n - Когда имеется набор составных объектов, и надо обеспечить единый интерфейс для их перебора " +
                "\n - Когда необходимо предоставить несколько альтернативных вариантов перебора одного и того же объекта";
        }
    }

    public class ItReader
    {
        private List<Book> books = new List<Book>();
        public Book[] GetBooks(Library library)
        {
            IBookIterator iterator = library.CreateNumerator();
            while (iterator.HasNext())
            {
                Book book = iterator.Next();
                books.Add(book);
            }
            return books.ToArray();
        }
    }

    public interface IBookIterator
    {
        bool HasNext();
        Book Next();
    }

    public interface IBookNumerable
    {
        IBookIterator CreateNumerator();
        int Count { get; }
        Book this[int index] { get; }
    }

    public class Book
    {
        public string Name { get; set; }
    }

    public class Library : IBookNumerable
    {

        private readonly Book[] _books;

        public Library()
        {
            _books = new[]
            {
                new Book{Name = "Адам Фримен ASP.NET CORE"},
                new Book{Name = "Джеффри Рихтер CLR  C#"}
            };
        }

        public IBookIterator CreateNumerator() => new LibraryNumerator(this);

        public int Count => _books.Length;

        public Book this[int index] => _books[index];
    }

    public class LibraryNumerator:IBookIterator
    {
        private readonly IBookNumerable _aggragate;
        private int _index;

        public LibraryNumerator(IBookNumerable a)
        {
            _aggragate = a;
        }

        public bool HasNext() => _index < _aggragate.Count;

        public Book Next() => _aggragate[_index++];
    }
}

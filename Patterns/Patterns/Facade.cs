﻿namespace Patterns.Patterns
{
    public class Facade : Pattern
    {
        public override string GetName() => "Facade";

        public override string GetDescription() =>
            "Фасад (Facade) представляет шаблон проектирования, который позволяет скрыть сложность системы с помощью предоставления упрощенного интерфейса для взаимодействия с ней." +
            "\n\n - Когда использовать фасад?" +
            "\n - Когда имеется сложная система, и необходимо упростить с ней работу.Фасад позволит определить одну точку взаимодействия между клиентом и системой." +
            "\n - Когда надо уменьшить количество зависимостей между клиентом и сложной системой. Фасадные объекты позволяют отделить, изолировать компоненты системы от клиента и развивать и работать с ними независимо." +
            "\n - Когда нужно определить подсистемы компонентов в сложной системе. Создание фасадов для компонентов каждой отдельной подсистемы позволит упростить взаимодействие между ними и повысить их независимость друг от друга.";
    }

    public class Potatoes
    {
        public string Plant() => "Посадить картошку";
    }

    public class Hoe
    {
        public string Spud() => "Окучивать землю";
    }

    public class Shovel
    {
        public string Dig() => "Копать";
    }

    public class Farm
    {
        private readonly Potatoes _potatoes;
        private readonly Hoe _hoe;
        private readonly Shovel _shovel;

        public Farm(Potatoes potatoes, Hoe hoe, Shovel shovel)
        {
            _potatoes = potatoes;
            _hoe = hoe;
            _shovel = shovel;
        }

        public string Grow() => _shovel.Dig() + " "+ _potatoes.Plant() + " "+ _hoe.Spud();
        public string DigUp() => _shovel.Dig();
    }
}

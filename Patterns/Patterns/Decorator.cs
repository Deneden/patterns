﻿namespace Patterns.Patterns
{
    public class Decorator : Pattern
    {
        public override string GetName() => "Decorator";

        public override string GetDescription() =>
            "Декоратор (Decorator) представляет структурный шаблон проектирования, который позволяет динамически подключать к объекту дополнительную функциональность." +
            "Для определения нового функционала в классах нередко используется наследование.Декораторы же предоставляет наследованию более гибкую альтернативу, поскольку позволяют динамически в процессе выполнения определять новые возможности у объектов." +
            "\n\n - Когда следует использовать декораторы?" +
            "\n - Когда надо динамически добавлять к объекту новые функциональные возможности.При этом данные возможности могут быть сняты с объекта" +
            "\n - Когда применение наследования неприемлемо.Например, если нам надо определить множество различных функциональностей и для каждой функциональности наследовать отдельный класс, то структура классов может очень сильно разрастись.Еще больше она может разрастись, если нам необходимо создать классы, реализующие все возможные сочетания добавляемых функциональностей.";
    }

    public abstract class Suitcase
    {
        public abstract string Open();
    }

    public class BeachSuitcase : Suitcase
    {
        public override string Open() => "Полотенце";
    }

    public abstract class SuitcaseDecorator : Suitcase
    {
        protected Suitcase Suitcase;
        protected SuitcaseDecorator(Suitcase suitcase) => Suitcase = suitcase;
        public override string Open() => Suitcase?.Open();
    }

    public class AddSlippers : SuitcaseDecorator
    {
        public AddSlippers(Suitcase suitcase) : base(suitcase)
        {
        }

        public override string Open() => base.Open()+" Тапочки";
    }

    public class AssGlasses : SuitcaseDecorator
    {
        public AssGlasses(Suitcase suitcase) : base(suitcase)
        {
        }

        public override string Open() => base.Open() + " Очки";
        
    }
}

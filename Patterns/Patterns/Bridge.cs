﻿namespace Patterns.Patterns
{
    public class Bridge : Pattern
    {
        public override string GetName() => "Bridge";

        public override string GetDescription() =>
            "Мост (Bridge) - структурный шаблон проектирования, который позволяет отделить абстракцию от реализации таким образом, чтобы и абстракцию, и реализацию можно было изменять независимо друг от друга." +
            "Даже если мы отделим абстракцию от конкретных реализаций, то у нас все равно все наследуемые классы будут жестко привязаны к интерфейсу, определяемому в базовом абстрактном классе.Для преодоления жестких связей и служит паттерн Мост." +
            "\n\n - Когда использовать данный паттерн?" +
            "\n - Когда надо избежать постоянной привязки абстракции к реализации" +
            "\n - Когда наряду с реализацией надо изменять и абстракцию независимо друг от друга. То есть изменения в абстракции не должно привести к изменениям в реализации";
    }

    public interface ITools
    {
        string Use();
    }

    public abstract class Worker
    {
        protected ITools Tool;
        public ITools Tools { set => Tool = value; }
        protected Worker(ITools tool) => Tool = tool;
        public virtual string UseTool() => Tool.Use();
        public abstract string DoWork();
    }

    public class RoofWorker : Worker
    {
        public RoofWorker(ITools tool) : base(tool)
        {
        }
        public override string DoWork() => "Чинить крышу";
    }

    public class LumberWorker : Worker
    {
        public LumberWorker(ITools tool) : base(tool)
        {
        }
        public override string DoWork() => "Заготовить лаги";
    }

    public class Hammer : ITools
    {
        public string Use() => "Стукнуть";
    }

    public class Saw : ITools
    {
        public string Use() => "Отпилить";
    }
}

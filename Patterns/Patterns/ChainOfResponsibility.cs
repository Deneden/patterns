﻿namespace Patterns.Patterns
{
    public class ChainOfResponsibility : Pattern
    {
        public override string GetName()
        {
            return "Chain of responsibility";
        }

        public override string GetDescription()
        {
            return
                "Цепочка Обязанностей (Chain of responsibility) - поведенческий шаблон проектирования, который позволяет избежать жесткой привязки отправителя запроса к получателю, позволяя нескольким объектам обработать запрос. Все возможные обработчики запроса образуют цепочку, а сам запрос перемещается по этой цепочке, пока один из ее объектов не обработает запрос. Каждый объект при получении запроса выбирает, либо обработать запрос, либо передать выполнение запроса следующему по цепочке." +
                "\n\n - Когда применяется цепочка обязанностей?" +
                "\n - Когда имеется более одного объекта, который может обработать определенный запрос" +
                "\n - Когда надо передать запрос на выполнение одному из нескольких объект, точно не определяя, какому именно объекту" +
                "\n - Когда набор объектов задается динамически";
        }
    }

    public abstract class OfficialHandler
    {
        public OfficialHandler Successor { get; set; }
        public abstract string HandleRequest(string request);
    }

    public class Financial : OfficialHandler
    {
        public override string HandleRequest(string request) 
            => request.Contains("деньги") 
            ? "Департамент финансов выделил деньги" 
            : Successor?.HandleRequest(request);
    }

    public class Workers : OfficialHandler
    {
        public override string HandleRequest(string request)
            => request.Contains("рабочие")
                ? "Строительный профсоюз выделил рабочих"
                : Successor?.HandleRequest(request);
    }

    public class Inspectors : OfficialHandler
    {
        public override string HandleRequest(string request)
            => request.Contains("проверка")
                ? "Инспекторы уже выехали"
                : Successor?.HandleRequest(request);
    }
}

﻿namespace Patterns.Patterns
{
    public class TemplateMethod : Pattern
    {
        public override string GetName()
        {
            return "Template Method";
        }

        public override string GetDescription()
        {
            return
                "Шаблонный метод (Template Method) определяет общий алгоритм поведения подклассов, позволяя им переопределить отдельные шаги этого алгоритма без изменения его структуры." +
                "\n\n - Когда использовать шаблонный метод?" +
                "\n - Когда планируется, что в будущем подклассы должны будут переопределять различные этапы алгоритма без изменения его структуры" +
                "\n - Когда в классах, реализующим схожий алгоритм, происходит дублирование кода. Вынесение общего кода в шаблонный метод уменьшит его дублирование в подклассах.";
        }
    }

    public abstract class Microwave
    {
        public string WarmUp() => Open()+" "+Put()+" "+Close()+ " "+On();
        
        public virtual string Open() => "Открываем дверцу";
        public abstract string Put();
        public virtual string Close() => "Закрываем дверцу";
        public virtual string On() => "Включаем";
    }

    public class Potato : Microwave
    {
        public override string Put() => "Кладём картошку";
    }

    public class Soup : Microwave
    {
        public override string Put() => "Ставим суп";
    }
}

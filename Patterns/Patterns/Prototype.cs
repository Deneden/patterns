﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Patterns.Patterns 
{
    public class Prototype : Pattern
    {
        public override string GetName()
        {
            return "Prototype";
        }

        public override string GetDescription()
        {
            return
                "Паттерн Прототип (Prototype) позволяет создавать объекты на основе уже ранее созданных объектов-прототипов. " +
                "То есть по сути данный паттерн предлагает технику клонирования объектов." +
                "\n\n - Когда использовать Прототип?" +
                "\n - Когда конкретный тип создаваемого объекта должен определяться динамически во время выполнения" +
                "\n - Когда нежелательно создание отдельной иерархии классов фабрик для создания объектов-продуктов из параллельной иерархии классов(как это делается, например, при использовании паттерна Абстрактная фабрика)" +
                "\n - Когда клонирование объекта является более предпочтительным вариантом нежели его создание и инициализация с помощью конструктора.Особенно когда известно, что объект может принимать небольшое ограниченное число возможных состояний.";
        }
    }

    public interface IOrder
    {
        object Clone();
        string GetInfo();
    }

    [Serializable]
    public class PrototypeOrderDetails
    {
        public int Table { get; set; }
        public int Cost { get; set; }

        public PrototypeOrderDetails(int table, int cost)
        {
            Table = table;
            Cost = cost;

        }
    }

    [Serializable]
    public class PrototypeSalad : IOrder
    {
        public PrototypeOrderDetails Details { get; }

        public PrototypeSalad(PrototypeOrderDetails details)
        {
            Details = details;
        }

        public object Clone()
        {
            object order;
            using (MemoryStream tmpStream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter(
                    null,
                    new StreamingContext(StreamingContextStates.Clone));

                formatter.Serialize(tmpStream,this);
                tmpStream.Seek(0, SeekOrigin.Begin);

                order = formatter.Deserialize(tmpStream);
            }
            return order;
        }

        public string GetInfo()
        {
            return $"Salad. Cost: {Details.Cost}, Table {Details.Table}";
        }
    }
}

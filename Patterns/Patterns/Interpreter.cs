﻿using System.Collections.Generic;

namespace Patterns.Patterns
{
    public class Interpreter : Pattern
    {
        public override string GetName()
        {
            return "Interpreter";
        }

        public override string GetDescription()
        {
            return
                "Паттерн Интерпретатор (Interpreter) определяет представление грамматики для заданного языка и " +
                "интерпретатор предложений этого языка. Как правило, данный шаблон проектирования применяется для часто повторяющихся операций.";
        }
    }

    public class InContext
    {
        private Dictionary<string, int> variables;

        public InContext() => variables = new Dictionary<string, int>();

        public int GetVariables(string name) => variables[name];

        public void SetVariables(string name, int value)
        {
            if (variables.ContainsKey(name)) variables[name] = value;
            else variables.Add(name, value);
        } 
    }

    public interface IExpression
    {
        int Interpret(InContext context);
    }

    public class NumberExpression : IExpression
    {
        private string _name;
        public NumberExpression(string varName) => _name = varName;
        public int Interpret(InContext context) => context.GetVariables(_name);
    }

    public class AddExpression : IExpression
    {
        private readonly IExpression _leftExpression;
        private readonly IExpression _rightExpression;

        public AddExpression(IExpression left, IExpression right)
        {
            _leftExpression = left;
            _rightExpression = right;
        }

        public int Interpret(InContext context) => _leftExpression.Interpret(context) +
                                                   _rightExpression.Interpret(context);
    }

    public class SubtractExpression : IExpression
    {
        private readonly IExpression _leftExpression;
        private readonly IExpression _rightExpression;

        public SubtractExpression(IExpression left, IExpression right)
        {
            _leftExpression = left;
            _rightExpression = right;
        }

        public int Interpret(InContext context) => _leftExpression.Interpret(context) -
                                                   _rightExpression.Interpret(context);
    }
}

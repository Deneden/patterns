﻿namespace Patterns.Patterns
{
    public class Proxy : Pattern
    {
        public override string GetName() => "Proxy";

        public override string GetDescription() =>
            "Паттерн Заместитель (Proxy) предоставляет объект-заместитель, который управляет доступом к другому объекту. То есть создается объект-суррогат, который может выступать в роли другого объекта и замещать его." +
            "\n\n - Когда использовать прокси?" +
            "\n - Когда надо осуществлять взаимодействие по сети, а объект-проси должен имитировать поведения объекта в другом адресном пространстве.Использование прокси позволяет снизить накладные издержки при передачи данных через сеть.Подобная ситуация еще называется удалённый заместитель(remote proxies)" +
            "\n - Когда нужно управлять доступом к ресурсу, создание которого требует больших затрат.Реальный объект создается только тогда, когда он действительно может понадобится, а до этого все запросы к нему обрабатывает прокси-объект.Подобная ситуация еще называется виртуальный заместитель (virtual proxies)" +
            "\n - Когда необходимо разграничить доступ к вызываемому объекту в зависимости от прав вызывающего объекта.Подобная ситуация еще называется защищающий заместитель(protection proxies)" +
            "\n - Когда нужно вести подсчет ссылок на объект или обеспечить потокобезопасную работу с реальным объектом.Подобная ситуация называется умные ссылки (smart reference)";
    }

    public class DbUser
    {
        public string Name { get; }
        public string Role { get; }

        public DbUser(string name, string role)
        {
            Name = name;
            Role = role;
        }
    }
    public interface IGetAccess
    {
        string Request();
    }

    public class DataBase : IGetAccess
    {
        public string Request() => "Ответ базы данных";
    }

    public class DataBaseProtector : IGetAccess
    {
        private DataBase _dataBase;
        private readonly DbUser _user;
        private readonly string _role;

        public DataBaseProtector(DbUser user, string role)
        {
            _user = user;
            _role = role;
        } 
        public string Request()
        {
            if (_user.Role == _role)
            {
                if (_dataBase == null) _dataBase = new DataBase();
                return _dataBase.Request();
            }
            return "Доступ закрыт";
        }
    }
}

﻿using System.Collections.Generic;

namespace Patterns.Patterns
{
    public class Flyweight : Pattern
    {
        public override string GetName() => "Flyweight";

        public override string GetDescription() =>
            "Паттерн Приспособленец (Flyweight) - структурный шаблон проектирования, который позволяет использовать разделяемые объекты сразу в нескольких контекстах. Данный паттерн используется преимущественно для оптимизации работы с памятью." +
            "\n\n - Паттерн Приспособленец следует применять при соблюдении всех следующих условий:" +
            "\n - Когда приложение использует большое количество однообразных объектов, из-за чего происходит выделение большого количества памяти" +
            "\n - Когда часть состояния объекта, которое является изменяемым, можно вынести во вне.Вынесение внешнего состояния позволяет заменить множество объектов небольшой группой общих разделяемых объектов.";
    }

    public abstract class Airplane
    {
        protected string Model;
        public abstract string Flight(string from, string to);
    }

    public class Boeing : Airplane
    {
        public Boeing() => Model = "Boeing 646";
        public override string Flight(string from, string to) => "Самолёт: " + Model + $" Рейс: {from} - {to}";
    }

    public class Airbus : Airplane
    {
        public Airbus() => Model = "Airbus A320";
        public override string Flight(string @from, string to) => "Самолёт: " + Model + $" Рейс: {from} - {to}";
    }

    public class AirplanesFactory
    {
        private Dictionary<string, Airplane> _airplanes = new Dictionary<string, Airplane>();

        public AirplanesFactory()
        {
            _airplanes.Add("Boeing", new Boeing());
            _airplanes.Add("Airbus", new Airbus());
        }

        public Airplane GetPlane(string key)
        {
            if (_airplanes.ContainsKey(key)) return _airplanes[key];
            return null;
        }
    }
}

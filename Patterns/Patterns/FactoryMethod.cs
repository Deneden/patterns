﻿namespace Patterns.Patterns
{
    public class FactoryMethod : Pattern
    {
        public override string GetName()
        {
            return "Factory Method";
        }

        public override string GetDescription()
        {
            return
                "Фабричный метод (Factory Method) - это паттерн, который определяет интерфейс для создания объектов некоторого " +
                "класса, но непосредственное решение о том, объект какого класса создавать происходит в подклассах. " +
                "То есть паттерн предполагает, что базовый класс делегирует создание объектов классам-наследникам." +
                "\n\n - Когда надо применять паттерн\nКогда заранее неизвестно, объекты каких типов необходимо создавать\n" +
                " - Когда система должна быть независимой от процесса создания новых объектов и расширяемой: " +
                "в нее можно легко вводить новые классы, объекты которых система должна создавать.\n" +
                " - Когда создание новых объектов необходимо делегировать из базового класса классам наследникам";
        }
    }

    public abstract class Phone
    {
        public abstract string Name { get; set; }
        public abstract int Price { get; set; }
    }

    public class IosPhone : Phone
    {
        public sealed override string Name { get; set; }
        public sealed override int Price { get; set; }

        public IosPhone(string name, int price)
        {
            Name = name;
            Price = price;
        }
    }

    public class AndroidPhone : Phone
    {
        public sealed override string Name { get; set; }
        public sealed override int Price { get; set; }

        public AndroidPhone(string name, int price)
        {
            Name = name;
            Price = price;
        }
    }

    public class WindowsPhone : Phone
    {
        public sealed override string Name { get; set; }
        public sealed override int Price { get; set; }

        public WindowsPhone(string name, int price)
        {
            Name = name;
            Price = price;
        }
    }

    public abstract class Developter
    {
        public string Name { get; set; }
        protected Developter(string name) => Name = name;
        public abstract Phone Developt();
    }

    public class AndroidDevelopter : Developter
    {
        public AndroidDevelopter(string name) : base(name)
        {
        }

        public override Phone Developt() => new AndroidPhone("Xiaomi Mi Note 2",25000);
    }

    public class IosDevelopter : Developter
    {
        public IosDevelopter(string name) : base(name)
        {
        }

        public override Phone Developt() => new IosPhone("IPhone 7", 55000);
    }

    public class WindowsDevelopter : Developter
    {
        public WindowsDevelopter(string name) : base(name)
        {
        }

        public override Phone Developt() => new WindowsPhone("Nokia Lumia 700", 20000);
    }
}

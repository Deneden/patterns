﻿namespace Patterns.Patterns
{
    public class Singletone : Pattern
    {
        public override string GetName()
        {
            return "Singletone";
        }

        public override string GetDescription()
        {
            return "Одиночка (Singleton, Синглтон) - порождающий паттерн, который гарантирует, " +
                   "что для определенного класса будет создан только один объект, а также предоставит к этому объекту точку доступа." +
                   "\n\n - Когда надо использовать Синглтон? " +
                   "\n - Когда необходимо, чтобы для класса существовал только один экземпляр";
        }
    }

    public class SingletinePassport
    {
        private static SingletinePassport _instance;
        private static readonly object SyncRoot = new object();
        public int Serial { get;  }

        private SingletinePassport(int serial)
        {
            Serial = serial;
        }

        public static SingletinePassport GetInstance(int serial)
        {
            if (_instance == null)
            {
                lock (SyncRoot)
                {
                    if (_instance == null) _instance = new SingletinePassport(serial);
                }
            }
            return _instance;
        }
    }
}

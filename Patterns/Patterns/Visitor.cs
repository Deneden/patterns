﻿using System.Collections.Generic;

namespace Patterns.Patterns
{
    public class Visitor : Pattern
    {
        public override string GetName() => "Visitor";

        public override string GetDescription() =>
            "Паттерн Посетитель (Visitor) позволяет определить операцию для объектов других классов без изменения этих классов." +
            "При использовании паттерна Посетитель определяются две иерархии классов: одна для элементов, для которых надо определить новую операцию, и вторая иерархия для посетителей, описывающих данную операцию." +
            "\n\n - Когда использовать данный паттерн?" +
            "\n -  Когда имеется много объектов разнородных классов с разными интерфейсами, и требуется выполнить ряд операций над каждым из этих объектов" +
            "\n - Когда классам необходимо добавить одинаковый набор операций без изменения этих классов" +
            "\n - Когда часто добавляются новые операции к классам, при этом общая структура классов стабильна и практически не изменяется";
    }

    public interface IService
    {
        void CheckBuggy(Buggy car);
        void CheckTruck(Truck car);
    }

    public interface ICar
    {
        void Accept(IService service);
    }

    public class Buggy : ICar
    {
        public string Name { get; set; }
        public string Diagnostic { get; set; }
        public string Color { get; set; }

        public Buggy(string name, string color)
        {
            Name = name;
            Color = color;
        } 
        public void Accept(IService service) => service.CheckBuggy(this);
    }

    public class Truck : ICar
    {
        public string Name { get; set; }
        public string Diagnostic { get; set; }
        public string Color { get; set; }

        public Truck(string name, string color)
        {
            Name = name;
            Color = color;
        }
        public void Accept(IService service) => service.CheckTruck(this);
    }

    public class CheckEngine : IService
    {
        public void CheckBuggy(Buggy car) => car.Diagnostic = "Двигатель работает исправно";
        public void CheckTruck(Truck car) => car.Diagnostic = "Двигатель требует ремонта";
    }

    public class PaintCar : IService
    {
        public void CheckBuggy(Buggy car) => car.Color = "Жёлтая";

        public void CheckTruck(Truck car) => car.Color = "Синий";
    }

    public class Service
    {
        readonly List<ICar> _cars = new List<ICar>();
        public void Add(ICar car) => _cars.Add(car);
        public void Remove(ICar car) => _cars.Remove(car);

        public void Accept(IService service)
        {
            foreach (var car in _cars)
            {
                car.Accept(service);
            }
        }
    }
}

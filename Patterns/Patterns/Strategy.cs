﻿namespace Patterns.Patterns
{
    public class Strategy : Pattern
    {
        public override string GetName()
        {
            return "Strategy";
        }

        public override string GetDescription()
        {
            return
                "Паттерн Стратегия (Strategy) представляет шаблон проектирования, который определяет набор алгоритмов, инкапсулирует каждый из них и обеспечивает их взаимозаменяемость. В зависимости от ситуации мы можем легко заменить один используемый алгоритм другим. При этом замена алгоритма происходит независимо от объекта, который использует данный алгоритм. " +
                "\n\n - Когда использовать стратегию? " +
                " \n- Когда есть несколько родственных классов, которые отличаются поведением. Можно задать один основной класс, а разные варианты поведения вынести в отдельные классы и при необходимости их применять " +
                "\n - Когда необходимо обеспечить выбор из нескольких вариантов алгоритмов, которые можно легко менять в зависимости от условий " +
                "\n - Когда необходимо менять поведение объектов на стадии выполнения программы " +
                "\n - Когда класс, применяющий определенную функциональность, ничего не должен знать о ее реализации";
        }
    }

    public interface IMoving
    {
        string Move();
    }

    public class StrategyMan : IMoving
    {
        public string Move()
        {
            return "Бежит";
        }
    }

    public class StrategyBird : IMoving
    {
        public string Move()
        {
            return "Летит";
        }
    }

    public class StrategyFish : IMoving
    {
        public string Move()
        {
            return "Плывёт";
        }
    }

    public class StrategyCreature
    {
        public IMoving Moving { get; }

        public StrategyCreature(IMoving moving) => Moving = moving;

        public string Move()
        {
            return Moving.Move();
        }
    }
}

﻿namespace Patterns.Patterns
{
    public class Command : Pattern
    {
        public override string GetName()
        {
            return "Command";
        }

        public override string GetDescription()
        {
            return
                "Паттерн Команда (Command) позволяет инкапсулировать запрос на выполнение определенного действия в виде отдельного объекта. Этот объект запроса на действие и называется командой. При этом объекты, инициирующие запросы на выполнение действия, отделяются от объектов, которые выполняют это действие." +
                "Команды могут использовать параметры, которые передают ассоциированную с командой информацию.Кроме того, команды могут ставиться в очередь и также могут быть отменены." +
                "\n\n - Когда использовать команды?" +
                "\n - Когда надо передавать в качестве параметров определенные действия, вызываемые в ответ на другие действия. То есть когда необходимы функции обратного действия в ответ на определенные действия." +
                "\n - Когда необходимо обеспечить выполнение очереди запросов, а также их возможную отмену." +
                "\n - Когда надо поддерживать логгирование изменений в результате запросов. Использование логов может помочь восстановить состояние системы - для этого необходимо будет использовать последовательность запротоколированных команд.";
        }
    }

    public interface ICommand
    {
        string Execute();
        string Undo();
    }

    public class AlarmButton : ICommand
    {
        private readonly Security _security;
        public AlarmButton(Security security) => _security = security;
        public string Execute() => _security?.Call();  
        public string Undo() => _security?.Cancel();
        
    }

    public class Security
    {
        public string Call() => "Охрана выехала";
        public string Cancel() => "Вызов охраны отменён";
      
    }

    public class Operator
    {
        private readonly ICommand _command;

        public Operator(ICommand command) => _command = command;
        public string CallSecurity() => _command?.Execute();
        public string CancelSecurity() => _command.Undo();
    }
}

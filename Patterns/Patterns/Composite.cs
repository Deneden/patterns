﻿using System.Collections.Generic;
using System.Linq;

namespace Patterns.Patterns
{
    public class Composite : Pattern
    {
        public override string GetName() => "Composite";

        public override string GetDescription() =>
            "Паттерн Компоновщик (Composite) объединяет группы объектов в древовидную структуру по принципу часть-целое и позволяет клиенту одинаково работать как с отдельными объектами, так и с группой объектов. Образно реализацию паттерна можно представить в виде меню, которое имеет различные пункты. Эти пункты могут содержать подменю, в которых, в свою очередь, также имеются пункты.То есть пункт меню служит с одной стороны частью меню, а с другой стороны еще одним меню.В итоге мы однообразно можем работать как с пунктом меню, так и со всем меню в целом." +
            "\n\n - Когда использовать компоновщик?" +
            "\n - Когда объекты должны быть реализованы в виде иерархической древовидной структуры" +
            "\n - Когда клиенты единообразно должны управлять как целыми объектами, так и их составными частями.То есть целое и его части должны реализовать один и тот же интерфейс";
    }

    public abstract class Component
    {
        protected string _name;
        public Component(string name) => _name = name;
        public abstract Component Add(Component c);
        public abstract void Remove(Component c);
        public abstract string Display();
    }

    public class CompositeMenu : Component
    {
        readonly List<Component> _children = new List<Component>();
        public CompositeMenu(string name) : base(name)
        {
        }

        public override Component Add(Component c)
        {
            _children.Add(c);
            return this;
        }


        public override void Remove(Component c) => _children.Remove(c);

        public override string Display()
        {
            var str = _name;
            if (_children.Any())
            {
                str += "{";
                foreach (Component component in _children)
                {
                    str += " " + component.Display();
                }
                str += "}";
            }
            return str;
        }
    }

    public class CompositeLeaf : Component
    {
        public CompositeLeaf(string name) : base(name)
        {
        }

        public override Component Add(Component c)
        {
            throw new System.NotImplementedException();
        }

        public override void Remove(Component c)
        {
            throw new System.NotImplementedException();
        }

        public override string Display() => _name;
    }
}

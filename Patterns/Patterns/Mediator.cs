﻿namespace Patterns.Patterns
{
    public class Mediator : Pattern
    {
        public override string GetName() => "Mediator";


        public override string GetDescription() =>
            "Паттерн Посредник (Mediator) представляет такой шаблон проектирования, который обеспечивает взаимодействие множества объектов без необходимости ссылаться друг на друга. Тем самым достигается слабосвязанность взаимодействующих объектов." +
            "\n\n - Когда используется паттерн Посредник?" +
            "\n - Когда имеется множество взаимосвязаных объектов, связи между которыми сложны и запутаны." +
            "\n - Когда необходимо повторно использовать объект, однако повторное использование затруднено в силу сильных связей с другими объектами.";
    }

    public abstract class MeMediator
    {
        public abstract void Send(string msg, MePerson worker);
    }

    public abstract class MePerson
    {
        protected MeMediator Mediator;
        public string LastMessage;
        protected MePerson(MeMediator mediator) => Mediator = mediator;
        public virtual void Send(string msg) => Mediator.Send(msg, this);
        public abstract string Notify(string msg);
    }

    public class Architector : MePerson
    {
        public Architector(MeMediator mediator) : base(mediator)
        {
        }
        public override string Notify(string msg) => LastMessage = "Для Архитектора: "+msg;
    }

    public class Estimator : MePerson
    {
        public Estimator(MeMediator mediator) : base(mediator)
        {
        }

        public override string Notify(string msg) => LastMessage = "Для Сметчика: " + msg;
    }

    public class MeBuilder : MePerson
    {
        public MeBuilder(MeMediator mediator) : base(mediator)
        {
        }

        public override string Notify(string msg) => LastMessage = "Для Строителя: " + msg;
    }

    public class Director : MeMediator
    {
        public MePerson Architector { get; set; }
        public MePerson Estimator { get; set; }
        public MePerson Builder { get; set; }

        public override void Send(string msg, MePerson worker)
        {
            if (Architector == worker) Estimator.Notify(msg);
            else if (Estimator == worker) Builder.Notify(msg);
            else if (Builder == worker) Architector.Notify(msg);
        }
    }
}

﻿
namespace Patterns.Patterns
{
    public class FluentBuilder : Pattern
    {
        public override string GetName()
        {
            return "Fluent Builder";
        }

        public override string GetDescription()
        {
            return
                "Паттерн Fluent Builder позволяет упростить процесс создания сложных объектов с помощью методов-цепочек, " +
                "которые наделяют объект каким-то определенным качеством. Применение данного паттерна делает процесс " +
                "конструирования объектов более прозрачным, а код более читабельным " +
                "Исторически fluent builder прежде всего позволял решить проблему перегруженных конструкторов.";
        }
    }
        
    public class FbUser
    {
        public string Name { get; set; }        // имя
        public string Company { get; set; }     // компания
        public int Age { get; set; }            // возраст
        public bool IsMarried { get; set; }     // женат/замужем
        public static FbUserBuilder Build => new FbUserBuilder(); //конструктор 
    }

    //внешний конструктор для класса FbUser
    public class FbUserBuilder
    {
        private readonly FbUser _user;

        public FbUserBuilder()
        {
            _user = new FbUser();
        }

        public FbUserBuilder SetName(string name)
        {
            _user.Name = name;
            return this;
        }

        public FbUserBuilder SetCompany(string company)
        {
            _user.Company = company;
            return this;
        }
        public FbUserBuilder SetAge(int age)
        {
            _user.Age = age > 0 ? age : 0;
            return this;
        }
        public FbUserBuilder IsMarried
        {
            get
            {
                _user.IsMarried = true;
                return this;
            }
        }

        public static implicit operator FbUser(FbUserBuilder builder) => builder._user; //неявное преобразования из FbUserBuilder в FbUser
    }
}

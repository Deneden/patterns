﻿using System.Collections.Generic;

namespace Patterns.Patterns
{
    public class Observer : Pattern
    {
        public override string GetName()
        {
            return "Observer";
        }

        public override string GetDescription()
        {
            return "Паттерн Наблюдатель (Observer) представляет поведенческий шаблон проектирования, который использует отношение \"один ко многим\". В этом отношении есть один наблюдаемый объект и множество наблюдателей. И при изменении наблюдаемого объекта автоматически происходит оповещение всех наблюдателей. " +
                   "Данный паттерн еще называют Publisher - Subscriber(издатель - подписчик), поскольку отношения издателя и подписчиков характеризуют действие данного паттерна: подписчики подписываются email - рассылку определенного сайта. Сайт - издатель с помощью email - рассылки уведомляет всех подписчиков о изменениях.А подписчики получают изменения и производят определенные действия: могут зайти на сайт, могут проигнорировать уведомления и т.д." +
                   "\n\n - Когда использовать паттерн Наблюдатель?" +
                   "\n - Когда система состоит из множества классов, объекты которых должны находиться в согласованных состояниях" +
                   "\n - Когда общая схема взаимодействия объектов предполагает две стороны: одна рассылает сообщения и является главным, другая получает сообщения и реагирует на них. Отделение логики обеих сторон позволяет их рассматривать независимо и использовать отдельно друга от друга." +
                   "\n - Когда существует один объект, рассылающий сообщения, и множество подписчиков, которые получают сообщения.При этом точное число подписчиков заранее неизвестно и процессе работы программы может изменяться.";
        }
    }

    interface IObservable
    {
        void AddObserver(IObserver o);
        void RemoveObserver(IObserver o);
        void NotifyObservers();
    }

    public interface IObserver
    {
        void Update(string s);
    }

    public class Reader : IObserver
    {
        public string Name { get; set; }
        public string Status { get; set; } = "not read yet";
        public void Update(string s)
        {
            Status = s;
        }
    }

    public class Reviewer : IObserver
    {
        public string Name { get; set; }
        public string Status { get; set; } = "not check yet";
        public void Update(string s)
        {
            Status = s;
        }
    }

    public class Publisher : IObservable
    {
        private readonly List<IObserver> _observers;
        public string Info { get; set; }

        public Publisher()
        {
            _observers = new List<IObserver>();
        }
        public void AddObserver(IObserver o)
        {
            _observers.Add(o);
        }

        public void RemoveObserver(IObserver o)
        {
            _observers.Remove(o);
        }

        public void NotifyObservers()
        {
            foreach (var o in _observers)
            {
                o.Update(Info);
            }
        }
    }
}

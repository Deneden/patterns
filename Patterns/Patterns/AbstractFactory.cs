﻿namespace Patterns.Patterns
{
    public class AbstractFactory : Pattern
    {
        public override string GetName() => "Abstract Factory";


        public override string GetDescription() =>
            "Паттерн Абстрактная фабрика (Abstract Factory) предоставляет интерфейс для создания семейств взаимосвязанных объектов с определенными интерфейсами без указания конкретных типов данных объектов. " +
            "\n\n - Когда использовать абстрактную фабрику" +
            "\n - Когда система не должна зависеть от способа создания и компоновки новых объектов" +
            "\n - Когда создаваемые объекты должны использоваться вместе и являются взаимосвязанными";
    }

    public abstract class AfCall
    {
        public abstract string Call();
    }

    class AfPhone : AfCall
    {
        public override string Call() => "Call using Phone";
    }

    class AfSkype : AfCall
    {
        public override string Call() => "Call using Skype";
    }

    public abstract class AfWrite
    {
        public abstract string Write();
    }

    class AfEmail : AfWrite
    {
        public override string Write() => "Write by Email";
    }

    class AfSms : AfWrite
    {
        public override string Write() => "Write by SMS";
    }

    public abstract class CommunicationFactory
    {
        public abstract AfCall CreateCall();
        public abstract AfWrite CreateWrite();
    }

    public class GsmCommunication : CommunicationFactory
    {
        public override AfCall CreateCall() => new AfPhone();
        public override AfWrite CreateWrite() => new AfSms();
    }

    public class InternetCommunication : CommunicationFactory
    {
        public override AfCall CreateCall() => new AfSkype();
        public override AfWrite CreateWrite() => new AfEmail();
    }

    public class AfClient
    {
        private readonly AfCall _call;
        private readonly AfWrite _write;

        public AfClient(CommunicationFactory factory)
        {
            _call = factory.CreateCall();
            _write = factory.CreateWrite();
        }

        public string Call() => _call.Call();
        public string Write() => _write.Write();
    }
}

﻿namespace Patterns.Patterns
{
    public class State : Pattern
    {
        public override string GetName()
        {
            return "State";
        }

        public override string GetDescription()
        {
            return
                "Состояние (State) - шаблон проектирования, который позволяет объекту изменять свое поведение в зависимости от внутреннего состояния." +
                "\n\n - Когда применяется данный паттерн?" +
                "\n - Когда поведение объекта должно зависеть от его состояния и может изменяться динамически во время выполнения" +
                "\n - Когда в коде методов объекта используются многочисленные условные конструкции, выбор которых зависит от текущего состояния объекта";
        }
    }

    public abstract class SwitcherState
    {
        public abstract string Handle(Switcher switcher);
    }

    public class Switcher
    {
        public SwitcherState State { get; set; }
        public Switcher(SwitcherState state) => State = state;
        public string Press() => State.Handle(this);

    }

    public class SwitcherOn : SwitcherState
    {
        public override string Handle(Switcher switcher)
        {
            switcher.State = new SwitcherOff();
            return "Свет Выключили";
        }
    }

    public class SwitcherOff : SwitcherState
    {
        public override string Handle(Switcher switcher)
        {
            switcher.State = new SwitcherOn();
            return "Свет Включили";
        }
    }
}

﻿namespace Patterns.Patterns
{
    public class Adapter : Pattern
    {
        public override string GetName() => "Adapter";

        public override string GetDescription() =>
            "Паттерн Адаптер (Adapter) предназначен для преобразования интерфейса одного класса в интерфейс другого. Благодаря реализации данного паттерна мы можем использовать вместе классы с несовместимыми интерфейсами." +
            "\n\n - Когда надо использовать Адаптер?" +
            "\n - Когда необходимо использовать имеющийся класс, но его интерфейс не соответствует потребностям" +
            "\n - Когда надо использовать уже существующий класс совместно с другими классами, интерфейсы которых не совместимы";
    }

    public interface IPetrol
    {
        string Pour();
    }

    public class Gasoline : IPetrol
    {
        public string Pour() => "Бензин";
    }

    public class GasStation
    {
        public string TankUp(IPetrol petrol) => petrol.Pour();
    }

    public interface IJuice
    {
        string Drink();
    }

    public class OrangeJuice : IJuice
    {
        public string Drink() => "Апельсиновый сок";
    }

    public class JuiceToPetroil : IPetrol
    {
        private readonly OrangeJuice _juice;
        public JuiceToPetroil(OrangeJuice juice) => _juice = juice;
        public string Pour() => _juice.Drink();
        
    }
}

﻿using System.Text;

namespace Patterns.Patterns
{
    public class Builder : Pattern
    {
        public override string GetName()
        {
            return "Builder";
        }

        public override string GetDescription()
        {
            return "Строитель (Builder) - шаблон проектирования, который инкапсулирует создание объекта и позволяет разделить его на различные этапы." +
                   "\n\n - Когда использовать паттерн Строитель? " +
                   "\n - Когда процесс создания нового объекта не должен зависеть от того, из каких частей этот объект состоит и как эти части связаны между собой" +
                   "\n - Когда необходимо обеспечить получение различных вариаций объекта в процессе его создания";
        }
    }

    public class BuilderColor
    {
        public string Color { get; set; }
    }

    public class BuilderSize
    {
        public string Size { get; set; }
    }

    public class BuilderLeaf
    {
        public string Leaf { get; set; }
    }

    public class Bflower
    {
        public BuilderColor Color { get; set; }
        public  BuilderSize Size { get; set; }
        public BuilderLeaf Leaf { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("Растение: ");
            if (Color != null) sb.Append(Color.Color + " ");
            if (Size != null) sb.Append(Size.Size + " ");
            if (Leaf != null) sb.Append(Leaf.Leaf);
            return sb.ToString();
        }
    }

    public abstract class BflowerBuilder
    {
        public Bflower Flower { get; private set; }

        public void CreateFlower()
        {
            Flower = new Bflower();
        }

        public abstract void SetColor();
        public abstract void SetSize();
        public abstract void SetLeaf();
    }

    public class Gardener
    {
        public Bflower Grow(BflowerBuilder builder)
        {
            builder.CreateFlower();
            builder.SetColor();
            builder.SetLeaf();
            builder.SetSize();
            return builder.Flower;
        }
    }

    public class CactusBuilder : BflowerBuilder
    {
        public override void SetColor()
        {
            Flower.Color = new BuilderColor {Color = "Зелёное"};
        }

        public override void SetSize()
        {
            Flower.Size = new BuilderSize { Size = "Маленькое" };
        }

        public override void SetLeaf()
        {
            Flower.Leaf = new BuilderLeaf { Leaf = "Без листьев" };
        }
    }

    public class DandelionBuilder : BflowerBuilder
    {
        public override void SetColor()
        {
            Flower.Color = new BuilderColor {Color = "Жёлтое"};
        }

        public override void SetSize()
        {
            Flower.Size = new BuilderSize {Size = "Среднее"};
        }

        public override void SetLeaf()
        {
            Flower.Leaf = new BuilderLeaf {Leaf = "С округлыми листьями"};
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Patterns.Patterns
{
    public class Memento : Pattern
    {
        public override string GetName() => "Memento";

        public override string GetDescription() =>
            "Паттерн Хранитель (Memento) позволяет выносить внутреннее состояние объекта за его пределы для последующего возможного восстановления объекта без нарушения принципа инкапсуляции." +
            "\n\n -  Когда использовать Memento?" +
            "\n - Когда нужно сохранить состояние объекта для возможного последующего восстановления" +
            "\n - Когда сохранение состояния должно проходить без нарушения принципа инкапсуляции";
    }

    public class Tester
    {
        public double InputData { get; set; }

        public double Result { get; private set; }

        public Tester(double data) => InputData = data;

        public void Experiment() => Result = Math.Sin(InputData);
 
        public TesterMomento SaveState() => new TesterMomento(InputData, Result);

        public void RestoreState(TesterMomento momento)
        {
            InputData = momento.InputData;
            Result = momento.Result;
        }
    }

    public class TesterMomento
    {
        public double InputData { get; }
        public double Result { get; }
        public TesterMomento(double inputData, double result)
        {
            InputData = inputData;
            Result = result;
        }
    }

    public class ExperimentsData
    {
        public List<TesterMomento> Data { get; }
        public ExperimentsData() => Data = new List<TesterMomento>();
    }
}

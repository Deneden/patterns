﻿using System;
using Patterns.Patterns;

namespace Patterns
{
    class Program
    {
        static void Main()
        {
            Pattern[] elements =
            {
                new FluentBuilder(),
                new FactoryMethod(),
                new AbstractFactory(), 
                new Singletone(), 
                new Prototype(), 
                new Builder(), 
                new Strategy(), 
                new Observer(), 
                new Command(), 
                new TemplateMethod(), 
                new Iterator(), 
                new State(), 
                new ChainOfResponsibility(), 
                new Interpreter(), 
                new Mediator(), 
                new Memento(), 
                new Visitor(), 
                new Decorator(), 
                new Adapter(), 
                new Facade(), 
                new Composite(), 
                new Proxy(), 
                new Bridge(), 
                new Flyweight()
            };

            Menu menu = new Menu(elements);

            
            string key;

            do
            {
                menu.Display();
                key = Console.ReadLine();

                for(var i = 0; i < elements.Length; i++)
                    if (key == (i + 1).ToString())
                    {
                        Console.Write(elements[i].GetDescription());
                        Console.WriteLine();
                    } 
            } while (key!="0");
        }
    }
}

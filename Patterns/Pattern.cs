﻿namespace Patterns
{
    public abstract class Pattern
    {
        public abstract string GetName();
        public abstract string GetDescription();
    }
}

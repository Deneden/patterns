﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class FacadeTests
    {
        [Fact]
        public void GrowPotatoEqual()
        {
            Farm farm = new Farm(new Potatoes(), new Hoe(), new Shovel());

            Assert.Equal("Копать Посадить картошку Окучивать землю", farm.Grow());
        }

        [Fact]
        public void DigUpPotatoEqual()
        {
            Farm farm = new Farm(new Potatoes(), new Hoe(), new Shovel());

            Assert.Equal("Копать", farm.DigUp());
        }
    }
}

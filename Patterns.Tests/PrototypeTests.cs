﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class PrototypeTests
    {
        [Fact]
        public void Clone()
        {
            PrototypeSalad salad = new PrototypeSalad(new PrototypeOrderDetails(1, 500));

            PrototypeSalad saladCopy = salad.Clone() as PrototypeSalad;

            salad.Details.Cost = 300;

            Assert.Equal("Salad. Cost: 500, Table 1",saladCopy.GetInfo());
        }
    }
}

﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class ObserverTests
    {
        [Fact]
        public void SubscribeReader()
        {
            Publisher publisher = new Publisher();
            Reader reader = new Reader {Name = "Michael"};


            publisher.AddObserver(reader);
            publisher.Info = "New Book";
            publisher.NotifyObservers();

            Assert.Equal("New Book", reader.Status);
        }

        [Fact]
        public void SubscribeReviewer()
        {
            Publisher publisher = new Publisher();
            Reviewer reviewer = new Reviewer { Name = "Checking Company" };


            publisher.AddObserver(reviewer);
            publisher.Info = "New Book";
            publisher.NotifyObservers();

            Assert.Equal("New Book", reviewer.Status);
        }
    }
}

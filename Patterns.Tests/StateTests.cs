﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class StateTests
    {
        [Fact]
        public void StateEqualString()
        {
            Switcher switcher = new Switcher(new SwitcherOn());

            Assert.Equal("Свет Выключили", switcher.Press());
        }

        [Fact]
        public void StateIsTypeOf()
        {
            Switcher switcher = new Switcher(new SwitcherOn());
            switcher.Press();

            Assert.IsAssignableFrom<SwitcherOff>(switcher.State);
        }
    }
}

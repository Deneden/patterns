﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class FlyweightTests
    {
        [Fact]
        public void BoeingEqual()
        {
            AirplanesFactory factory = new AirplanesFactory();

            Airplane plane = factory.GetPlane("Boeing");
            var planeFly = plane?.Flight("Tokyo", "Moscow");

            plane = factory.GetPlane("Airbus");
            plane?.Flight("New York", "Hongkong");

            Assert.Equal("Самолёт: Boeing 646 Рейс: Tokyo - Moscow", planeFly);
        }

        [Fact]
        public void AirbusEqual()
        {
            AirplanesFactory factory = new AirplanesFactory();

            Airplane plane = factory.GetPlane("Airbus");
            var planeFly = plane?.Flight("New York", "Hongkong");

            Assert.Equal("Самолёт: Airbus A320 Рейс: New York - Hongkong", planeFly);
        }
    }
}

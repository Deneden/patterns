﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class ProxyTests
    {
        [Fact]
        public void DenyEqual()
        {
            DbUser user = new DbUser("Вася","Guest");
            IGetAccess dataBase = new DataBaseProtector(user,"Admin");
            Assert.Equal("Доступ закрыт", dataBase.Request());
        }

        [Fact]
        public void AllowEqual()
        {
            DbUser user = new DbUser("Вася", "Admin");
            IGetAccess dataBase = new DataBaseProtector(user, "Admin");
            Assert.Equal("Ответ базы данных", dataBase.Request());
        }
    }
}

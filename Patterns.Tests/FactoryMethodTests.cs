﻿
using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class FactoryMethodTests
    {
        [Fact]
        public void EqualFmAndroid()
        {
            Developter developter = new AndroidDevelopter("China");
            Phone phone1 = developter.Developt();

            Assert.Equal("Xiaomi Mi Note 2", phone1.Name);
            Assert.Equal(25000, phone1.Price);
        }

        [Fact]
        public void EqualFmIos()
        {
            Developter developter = new IosDevelopter("USA");
            Phone phone2 = developter.Developt();

            Assert.Equal("IPhone 7", phone2.Name);
            Assert.Equal(55000, phone2.Price);
        }

        [Fact]
        public void EqualFmWindows()
        {
            Developter developter = new WindowsDevelopter("Russia");
            Phone phone3 = developter.Developt();

            Assert.Equal("Nokia Lumia 700", phone3.Name);
            Assert.Equal(20000, phone3.Price);
        }
    }
}

﻿using System;
using System.Linq;
using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class MementoTests
    {
        [Fact]
        public void MementoEqual()
        {
            ExperimentsData data = new ExperimentsData();
            Tester tester = new Tester(0.5);

            tester.Experiment();

            data.Data.Add(tester.SaveState());

            tester.InputData = 0.1;
            tester.Experiment();
            tester.RestoreState(data.Data.LastOrDefault());

            Assert.Equal(Math.Sin(0.5),tester.Result);
        }
    }
}

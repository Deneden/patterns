﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class ChainOfResponsibilityTEsts
    {
        [Fact]
        public void FinancialEqual()
        {
            OfficialHandler financial = new Financial();
            OfficialHandler workers = new Workers();
            OfficialHandler inspectors = new Inspectors();

            financial.Successor = workers;
            workers.Successor = inspectors;

            Assert.Equal("Департамент финансов выделил деньги", financial.HandleRequest("на строительство нужны деньги"));
        }

        [Fact]
        public void WorkersEqual()
        {
            OfficialHandler financial = new Financial();
            OfficialHandler workers = new Workers();
            OfficialHandler inspectors = new Inspectors();

            financial.Successor = workers;
            workers.Successor = inspectors;

            Assert.Equal("Строительный профсоюз выделил рабочих", financial.HandleRequest("на объект нужны рабочие"));
        }


        [Fact]
        public void InspectorsEqual()
        {
            OfficialHandler financial = new Financial();
            OfficialHandler workers = new Workers();
            OfficialHandler inspectors = new Inspectors();

            financial.Successor = workers;
            workers.Successor = inspectors;

            Assert.Equal("Инспекторы уже выехали", financial.HandleRequest("требуется проверка объекта"));
        }
    }
}

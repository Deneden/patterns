﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class TemplateMethodTests
    {
        [Fact]
        public void EqualPotato()
        {
            Potato potato = new Potato();

            Assert.Equal("Открываем дверцу Кладём картошку Закрываем дверцу Включаем",potato.WarmUp());
        }

        [Fact]
        public void EqualSoup()
        {
            Soup soup = new Soup();

            Assert.Equal("Открываем дверцу Ставим суп Закрываем дверцу Включаем", soup.WarmUp());
        }
    }
}

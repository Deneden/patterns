﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class SingletoneTests
    {
        [Fact]
        public void ChangeSingletone()
        {
            SingletinePassport.GetInstance(100953);

            var passport = SingletinePassport.GetInstance(999999);

            Assert.Equal(100953,passport.Serial);
        }
    }
}

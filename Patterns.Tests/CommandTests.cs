﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class CommandTests
    {
        [Fact]
        public void EqualCall()
        {
            var security = new Security();          
            var oper = new Operator(new AlarmButton(security));

            Assert.Equal("Охрана выехала",oper.CallSecurity());
        }

        [Fact]
        public void EqualUndo()
        {
            var security = new Security();
            var oper = new Operator(new AlarmButton(security));

            Assert.Equal("Вызов охраны отменён", oper.CancelSecurity());
        }
    }
}

﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class StrategyTests
    {
        [Fact]
        public void ManCreate()
        {
            StrategyCreature man = new StrategyCreature(new StrategyMan());

            Assert.Equal("Бежит", man.Move());
        }

        [Fact]
        public void BirdCreate()
        {
            StrategyCreature bird = new StrategyCreature(new StrategyBird());

            Assert.Equal("Летит", bird.Move());
        }

        [Fact]
        public void FishCreate()
        {
            StrategyCreature fish = new StrategyCreature(new StrategyFish());

            Assert.Equal("Плывёт", fish.Move());
        }
    }
}

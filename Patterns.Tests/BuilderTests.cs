﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class BuilderTests
    {
        [Fact]
        public void BuildCactus()
        {
            Gardener gardener = new Gardener();

            CactusBuilder builder = new CactusBuilder();

            Bflower cactus = gardener.Grow(builder);

            Assert.Equal("Растение: Зелёное Маленькое Без листьев", cactus.ToString());
        }

        [Fact]
        public void BuildDandelion()
        {
            Gardener gardener = new Gardener();

            DandelionBuilder builder = new DandelionBuilder();

            Bflower dandelion = gardener.Grow(builder);

            Assert.Equal("Растение: Жёлтое Среднее С округлыми листьями", dandelion.ToString());
        }
    }
}

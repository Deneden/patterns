﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class VisitorTests
    {
        [Fact]
        public void EngineEqual()
        {
            var buggy = new Buggy("Багги-1","Красная");
            var truck = new Truck("MAN","Чёрный");
            Service service = new Service();
            service.Add(buggy);
            service.Add(truck);
            service.Accept(new CheckEngine());

            Assert.Equal("Двигатель работает исправно", buggy.Diagnostic);
            Assert.Equal("Двигатель требует ремонта", truck.Diagnostic);
        }

        [Fact]
        public void PaintEqual()
        {
            var buggy = new Buggy("Багги-1","Красная");
            var truck = new Truck("MAN","Чёрный");
            Service service = new Service();
            service.Add(buggy);
            service.Add(truck);
            service.Accept(new PaintCar());

            Assert.Equal("Жёлтая", buggy.Color);
            Assert.Equal("Синий", truck.Color);
        }
    }
}

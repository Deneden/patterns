﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class AdapterTests
    {
        [Fact]
        public void TankUpEqual()
        {
            GasStation station = new GasStation();
            OrangeJuice juice = new OrangeJuice();

            IPetrol petrol = new JuiceToPetroil(juice);
            Assert.Equal("Апельсиновый сок",station.TankUp(petrol));
        }
    }
}

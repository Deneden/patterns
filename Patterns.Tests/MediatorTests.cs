﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class MediatorTests
    {
        [Fact]
        public void EstimatorEqual()
        {
            Director director = new Director();
            MePerson architector = new Architector(director);
            MePerson estimator = new Estimator(director);
            MePerson builder = new MeBuilder(director);

            director.Architector = architector;
            director.Estimator = estimator;
            director.Builder = builder;

            architector.Send("Проект готов, нужно посчитать смету");

            Assert.Equal("Для Сметчика: Проект готов, нужно посчитать смету", estimator.LastMessage);
        }

        [Fact]
        public void BuilderEqual()
        {
            Director director = new Director();
            MePerson architector = new Architector(director);
            MePerson estimator = new Estimator(director);
            MePerson builder = new MeBuilder(director);

            director.Architector = architector;
            director.Estimator = estimator;
            director.Builder = builder;

            estimator.Send("Смета посчитана, пора строить");

            Assert.Equal("Для Строителя: Смета посчитана, пора строить", builder.LastMessage);
        }

        [Fact]
        public void ArchitectorEqual()
        {
            Director director = new Director();
            MePerson architector = new Architector(director);
            MePerson estimator = new Estimator(director);
            MePerson builder = new MeBuilder(director);

            director.Architector = architector;
            director.Estimator = estimator;
            director.Builder = builder;

            builder.Send("Объект построен, можно проверять");

            Assert.Equal("Для Архитектора: Объект построен, можно проверять", architector.LastMessage);
        }
    }
}

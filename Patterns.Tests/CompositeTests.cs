﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class CompositeTests
    {
        [Fact]
        public void MenuEqual()
        {
            Component menu = new CompositeMenu("Root");
            Component leaf = new CompositeLeaf("Home");
            Component projects = new CompositeMenu("Projects");
            Component project1 = new CompositeLeaf("Project1");
            Component project2 = new CompositeLeaf("Project2");

            projects.Add(project1).Add(project2);
            menu.Add(leaf).Add(projects);

            Assert.Equal("Root{ Home Projects{ Project1 Project2}}",menu.Display());
            
        }
    }
}

﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class BridgeTests
    {
        [Fact]
        public void RoofWorkerEqual()
        {
            Worker roofWorker = new RoofWorker(new Saw());
            roofWorker.Tools = new Hammer();
            Assert.Equal("Стукнуть",roofWorker.UseTool());
            Assert.Equal("Чинить крышу",roofWorker.DoWork());
        }

        [Fact]
        public void LumberWorkerEqual()
        {
            Worker lumberWorker = new LumberWorker(new Saw());
            Assert.Equal("Отпилить", lumberWorker.UseTool());
            Assert.Equal("Заготовить лаги", lumberWorker.DoWork());
        }
    }
}

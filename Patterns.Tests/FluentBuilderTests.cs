﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class FluentBuilderTests
    {
        [Fact]
        public void EqualFbUser()
        {
            FbUser user = FbUser.Build.SetName("Alex").SetAge(16).SetCompany("Toyota");

            Assert.Equal("Alex", user.Name);
            Assert.Equal(16,user.Age);
            Assert.Equal("Toyota", user.Company);
            Assert.False(user.IsMarried);
        }
    }
}

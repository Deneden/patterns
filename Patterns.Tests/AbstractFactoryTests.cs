﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class AbstractFactoryTests
    {
        [Fact]
        public void EqualGsmFactory()
        {
            AfClient client = new AfClient(new GsmCommunication());

            Assert.Equal("Call using Phone",client.Call());
            Assert.Equal("Write by SMS", client.Write());
        }

        [Fact]
        public void EqualInternetFactory()
        {
            AfClient client = new AfClient(new InternetCommunication());

            Assert.Equal("Call using Skype", client.Call());
            Assert.Equal("Write by Email", client.Write());
        }
    }
}

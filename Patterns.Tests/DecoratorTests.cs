﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class DecoratorTests
    {
        [Fact]
        public void DecoratorEqual()
        {
            Suitcase suitcase = new BeachSuitcase();
            suitcase = new AddSlippers(suitcase);
            suitcase = new AssGlasses(suitcase);

            Assert.Equal("Полотенце Тапочки Очки",suitcase.Open());
        }
    }
}

﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class IteratorTests
    {
        [Fact]
        public void BooksEqual()
        {
            Library library = new Library();
            ItReader reader = new ItReader();

            Book[] books = reader.GetBooks(library);

            Book[] testBooks  = 
            {
                new Book{Name = "Адам Фримен ASP.NET CORE"},
                new Book{Name = "Джеффри Рихтер CLR  C#"}
            };

            Assert.Equal(testBooks[0].Name, books[0].Name);
            Assert.Equal(testBooks[1].Name, books[1].Name);
        }
    }
}

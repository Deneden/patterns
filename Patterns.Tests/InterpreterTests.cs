﻿using Patterns.Patterns;
using Xunit;

namespace Patterns.Tests
{
    public class InterpreterTests
    {
        [Fact]
        public void CalculateEqual()
        {
            int x = 5;
            int y = 2;
            int z = 3;

            InContext context = new InContext();

            context.SetVariables("x",x);
            context.SetVariables("y",y);
            context.SetVariables("z",z);

            IExpression expression = new SubtractExpression(
                new AddExpression(
                    new NumberExpression("x"),new NumberExpression("y") ),
                new NumberExpression("z"));

            int result = expression.Interpret(context);

            Assert.Equal(4,result);
        }
    }
}
